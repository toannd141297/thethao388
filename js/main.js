$(document).ready(function () {
  var menu = $("#primary");
  var menulink = $(".menu-link");
  var menumobile = $(".header-mobile");

  menulink.click(function () {
    menulink.toggleClass("active");
    menu.toggleClass("active");
    menumobile.toggleClass("header-mobile-active");
    return false;
  });
});

$(".language").click(function () {
  if ($(".language .list-lang").hasClass("active")) {
    $(".language .list-lang").removeClass("active");
  } else {
    $(".language .list-lang").addClass("active");
  }
});

$(".list-lang li").click(function () {
  const lang = $(this).data("lang");
  $(".lg-choose").html(lang);
  $("#langCode").val(lang);
  setTimeout(translate(), 1000);
});

function translate() {
  var langCode = $("#langCode").val() || "en";
  $.getJSON("../lang/" + langCode + ".json", function (data) {
    $("[langKey]").each(function (index) {
      var strTr = data[$(this).attr("langKey")];
      $(this).html(strTr);
    });
  }).fail(function () {
    console.log("An error has occurred.");
  });
}

$(document).click(function (event) {
  if (!$(event.target).closest(".language").length) {
    $(".language .list-lang").removeClass("active");
  }
});
$(document).ready(function () {
  $(".btn-chat").on("click", function () {
    $(this).toggleClass("active");
    $("#chat").toggle();
  });
  $(".btn-close").on("click", function () {
    $(".btn-chat").removeClass("active");
    $("#chat").hide();
  });
  $(".btn-more").on("click", function () {
    $(this).toggleClass("active");
    // $("#des").toggle();
    $(".des").toggleClass("des-active");

  });
});
dragElement(document.getElementById("chat"));

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    /* if present, the header is where you move the DIV from:*/
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    /* otherwise, move the DIV from anywhere inside the DIV:*/
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}