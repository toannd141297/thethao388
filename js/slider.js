$(".left--list").slick({
  dots: true,
  infinite: true,
  variableWidth: true,
  centerPadding: "60px",
  autoplay: true,
  autoplaySpeed: 2000,
});
$(".hero--list").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  infinite: true,
  // variableWidth: true,
});
